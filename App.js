import 'react-native-gesture-handler';
import React from 'react';
// Redux
import {Provider} from 'react-redux';
import {store, persistor} from './src/redux/Store';
import {PersistGate} from 'redux-persist/integration/react';
// Navigation
import {NavigationContainer} from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import {RootNavigator} from './src/navigation';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import LottieView from 'lottie-react-native';

const App = () => {
  return (
    <Provider store={store}>
      <IconRegistry icons={EvaIconsPack} />
      <PersistGate
        loading={
          <LottieView
            source={require('./src/assets/lottie/loading.json')}
            autoPlay
            loop
          />
        }
        persistor={persistor}>
        <NavigationContainer>
          <ApplicationProvider {...eva} theme={eva.light}>
            <RootNavigator />
          </ApplicationProvider>
          <Toast ref={ref => Toast.setRef(ref)} />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
