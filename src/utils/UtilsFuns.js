import Toast from 'react-native-toast-message';
export const randomDate = (start, end) => {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime()),
  );
};

export const randomId = () => Math.floor(Math.random() * 1000 + 1);

export function ShowToast(
  type = 'error',
  message = 'Error deconocido',
  title = 'Error ocurrido',
) {
  Toast.show({
    type: type,
    position: 'top',
    text1: title,
    text2: message,
    visibilityTime: 5000,
    autoHide: true,
    topOffset: 15,
    bottomOffset: 40,
    onShow: () => {},
    onHide: () => {},
  });
}
