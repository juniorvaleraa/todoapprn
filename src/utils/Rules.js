export const taskTitleRules = {
  required: {value: true, message: 'Ingresa titulo de la tarea'},
};

export const descriptionTaskRules = {
  required: {value: true, message: 'Ingresa la descripcion de la tarea'},
};
