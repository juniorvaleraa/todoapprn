import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {typografy, margin} from '../../Theme';

export const ErrorMessage = ({msg}) => {
  return (
    <Text
      style={{
        marginVertical: margin.small,
        color: 'red',
        letterSpacing: 1,
        alignSelf: 'flex-start',
        ...typografy.caption,
      }}>
      {msg}
    </Text>
  );
};
