import React, {useState, useEffect} from 'react';
import {TouchableOpacity, Keyboard, View, StyleSheet} from 'react-native';
import {colors, sizes} from '../../Theme';

export const PlusTabBarButton = ({children, onPress}) => {
  const [keyboardStatus, setKeyboardStatus] = useState(false);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => setKeyboardStatus(true);
  const _keyboardDidHide = () => setKeyboardStatus(false);

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.buttonLayout,
        // eslint-disable-next-line react-native/no-inline-styles
        {top: keyboardStatus ? 10 : -sizes.giant / 2.5},
      ]}>
      <View style={styles.button}>{children}</View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonLayout: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    height: sizes.giant,
    width: sizes.giant,
    elevation: 6,
    borderRadius: sizes.giant / 2,
    backgroundColor: colors.primary,
  },
});
