import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {colors, sizes, typografy} from '../../Theme';

export const PrimaryButton = ({text, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: sizes.large,
        backgroundColor: colors.primary,
        borderRadius: 6,
      }}>
      <Text style={{color: 'white', ...typografy.title}}>{text}</Text>
    </TouchableOpacity>
  );
};
