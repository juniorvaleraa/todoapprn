import React, {useState, useEffect} from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import {
  SearchIcon,
  FilterByIcon,
  TimeIcon,
  StarIcon,
  AllDoneIcon,
} from '../Icons/EvaIcons';

import {colors, sizes} from '../../Theme';
import {useForm, Controller} from 'react-hook-form';
import {MenuItem, OverflowMenu} from '@ui-kitten/components';
import {ErrorMessage} from '../Messages/ErrorMessage';
import {useSelector, useDispatch} from 'react-redux';
import {setFilteredTask} from '../../redux/actions/taskActions';
import _ from 'lodash';

const filter = (task, query) =>
  task.title.toLowerCase().includes(query.toLowerCase());

const sortByName = (array, query) => {
  const results = array.filter(task => filter(task, query));
  return results;
};

export const FilterByBar = ({showFilter, setShowFilter}) => {
  const Dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState('');
  const tasks = useSelector(state => state.tasks.tasks);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const filtered = useSelector(state => state.tasks.filteredTasks);

  const {
    control,
    handleSubmit,
    formState: {errors},
    getValues,
  } = useForm();

  useEffect(() => {
    if (searchText.length === 0) setShowFilter(false);
  }, [searchText]);

  const onSelect = index => {
    const filterByCompletedTask = 0;
    const filterByFavoriteTask = 1;
    const filterByDateTask = 2;

    if (index.row === filterByCompletedTask)
      Dispatch(setFilteredTask(_.filter(tasks, {completed: true})));

    if (index.row === filterByFavoriteTask)
      Dispatch(setFilteredTask(_.filter(tasks, {favorite: true})));

    if (index.row === filterByDateTask)
      Dispatch(setFilteredTask(_.sortBy(tasks, 'date')));

    setSelectedIndex(index);
    setVisible(false);
    setShowFilter(true);
  };

  const onSearch = () => {
    let results;
    if (showFilter) results = sortByName(filtered, searchText);
    else results = sortByName(tasks, searchText);

    Dispatch(setFilteredTask(results));
    setShowFilter(true);
  };

  const FilterByButton = () => {
    return (
      <TouchableOpacity onPress={setMenuVisible}>
        <FilterByIcon color={colors.primary} size={sizes.small} />
      </TouchableOpacity>
    );
  };

  const onChangeSearch = text => setSearchText(text);

  const setMenuVisible = () => setVisible(prev => !prev);

  return (
    <View style={{flexDirection: 'row', flex: 0.12}}>
      <View
        style={{
          flex: 0.8,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
        <TouchableOpacity onPress={onSearch}>
          <SearchIcon color={colors.primary} size={sizes.normal} />
        </TouchableOpacity>
        <View style={{width: '100%'}}>
          <TextInput
            placeholder="Filtra tus tareas"
            value={searchText}
            onSubmitEditing={onSearch}
            onChangeText={onChangeSearch}
            maxLength={40}
          />
        </View>
      </View>
      <View
        style={{
          flex: 0.2,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <OverflowMenu
          anchor={FilterByButton}
          visible={visible}
          selectedIndex={selectedIndex}
          onSelect={onSelect}
          onBackdropPress={setMenuVisible}>
          <MenuItem title="Completados" accessoryLeft={AllDoneIcon} />
          <MenuItem title="Favoritos" accessoryLeft={StarIcon} />
          <MenuItem title="Fecha" accessoryLeft={TimeIcon} />
        </OverflowMenu>
      </View>
    </View>
  );
};
