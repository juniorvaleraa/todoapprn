/* eslint-disable prettier/prettier */
import React from 'react';
import {View, SafeAreaView, StyleSheet} from 'react-native';
import {colors} from '../../Theme';

export const SafeAreaViewContainer = ({children}) => (
  <SafeAreaView style={styles.container}>
    <View style={styles.container}>{children}</View>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light,
    padding: 2,
  },
});
