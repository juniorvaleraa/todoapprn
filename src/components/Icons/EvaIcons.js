import React from 'react';
import {Icon} from '@ui-kitten/components';
import {colors, sizes} from '../../Theme';

export const PlusIcon = ({size, color = 'white', ...props}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="plus-outline"
    />
  );
};

export const ListIcon = ({size, name, color, ...props}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="list-outline"
    />
  );
};

export const StarIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="star-outline"
    />
  );
};

export const SearchIcon = ({size, name, color, ...props}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="search-outline"
    />
  );
};

export const FilterByIcon = ({size, name, color, ...props}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="flip"
    />
  );
};

export const TimeIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="clock-outline"
    />
  );
};

export const CheckIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="checkmark-outline"
    />
  );
};

export const AllDoneIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="done-all-outline"
    />
  );
};

export const CompletedIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="checkmark-square-outline"
    />
  );
};

export const DeleteIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="trash-2-outline"
    />
  );
};

export const FilledStarIcon = ({
  size = sizes.small,
  name,
  color = colors.primary,
  ...props
}) => {
  return (
    <Icon
      {...props}
      fill={color}
      style={{width: size, height: size}}
      name="star"
    />
  );
};
