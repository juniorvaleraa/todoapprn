import * as taskType from '../types/Types';
import {randomDate} from '../../utils/UtilsFuns';
const initialState = {
  tasks: [
    {
      id: 1,
      title: 'Task 1',
      description: 'Description 1',
      completed: false,
      favorite: false,
      date: randomDate(new Date(2021, 1, 1), new Date()),
    },
    {
      id: 2,
      title: 'Task 2',
      description: 'Description 2',
      completed: false,
      favorite: false,
      date: randomDate(new Date(2021, 1, 1), new Date()),
    },
    {
      id: 3,
      title: 'Task 3',
      description: 'Description 3',
      completed: false,
      favorite: false,
      date: randomDate(new Date(2021, 1, 1), new Date()),
    },
    {
      id: 4,
      title: 'Task 4',
      description: 'Description 4',
      completed: false,
      favorite: false,
      date: randomDate(new Date(2021, 1, 1), new Date()),
    },
    {
      id: 5,
      title: 'Task 5',
      description: 'Description 5',
      completed: false,
      favorite: false,
      date: randomDate(new Date(2021, 1, 1), new Date()),
    },
  ],

  filteredTasks: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case taskType.CREATE_TASK:
      return {
        ...state,
        tasks: [action.task, ...state.tasks],
      };
    case taskType.DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(i => i.id !== action.id),
      };

    case taskType.EDIT_TASK:
      return {
        ...state,
        tasks: [
          ...state.tasks.filter(i => i.id !== action.task.id),
          action.task,
        ],
      };

    case taskType.SET_FAVORITE:
      return {
        ...state,
        tasks: [
          {...action.task, favorite: true},
          ...state.tasks.filter(i => i.id !== action.task.id),
        ],
      };

    case taskType.DELETE_FAVORITE:
      return {
        ...state,
        tasks: [
          ...state.tasks.filter(i => i.id !== action.task.id),
          {...action.task, favorite: false},
        ],
      };

    case taskType.SET_FILTERED_TASK:
      return {
        ...state,
        filteredTasks: [...action.tasks],
      };

    case taskType.COMPLETED_TASK:
      return {
        ...state,
        tasks: [
          ...state.tasks.filter(i => i.id !== action.task.id),
          {...action.task, completed: true},
        ],
      };

    default:
      return state;
  }
};
