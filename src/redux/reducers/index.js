import {combineReducers} from 'redux';
import taskReducer from './TaskReducer';

const appReducer = combineReducers({
  tasks: taskReducer,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
