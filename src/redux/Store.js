import {createStore, applyMiddleware} from 'redux';
import rootReducers from './reducers';
// Middleware

import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
// Persist
import {persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navigation'],
};

const middleware = applyMiddleware(promise, thunk);
const persistedReducer = persistReducer(persistConfig, rootReducers);
const store = createStore(persistedReducer, middleware);
const persistor = persistStore(store);

export {store, persistor};
