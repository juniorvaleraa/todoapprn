import {
  COMPLETED_TASK,
  CREATE_TASK,
  EDIT_TASK,
  DELETE_TASK,
  SET_FILTERED_TASK,
  SET_FAVORITE,
  DELETE_FAVORITE,
} from '../types/Types';

export const createTask = task => {
  return {
    type: CREATE_TASK,
    task,
  };
};

export const editTask = task => {
  return {
    type: EDIT_TASK,
    task,
  };
};

export const deleteTask = ({id}) => {
  return {
    type: DELETE_TASK,
    id,
  };
};

export const setFavorite = task => {
  return {
    type: SET_FAVORITE,
    task,
  };
};

export const deleteFavorite = task => {
  return {
    type: DELETE_FAVORITE,
    task,
  };
};

export const setFilteredTask = tasks => {
  return {
    type: SET_FILTERED_TASK,
    tasks,
  };
};

export const completeTask = task => {
  return {
    type: COMPLETED_TASK,
    task,
  };
};
