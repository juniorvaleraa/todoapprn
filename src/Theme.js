import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const typografy = {
  title: {
    fontSize: wp(5),
    letterSpacing: 1,
  },
  subtitle: {
    fontSize: wp(4),
    letterSpacing: 1,
  },
  caption: {
    fontSize: wp(3.3),
    letterSpacing: 1,
  },
  tiny: {
    fontSize: wp(2.2),
    letterSpacing: 1,
  },
  link: {
    fontSize: 15,
    letterSpacing: 1,
  },
};

export const padding = {
  small: wp(1),
  normal: wp(2),
  large: wp(3),
  giant: wp(4),
};

export const margin = {
  small: hp(1),
  normal: hp(2),
  large: hp(3),
  giant: hp(4),
};

export const sizes = {
  small: wp(6),
  normal: wp(8),
  large: wp(10),
  giant: wp(16),
};

export const colors = {
  primary: '#ed1a4f',
  secondary: '#009cdf',
  light: 'whitesmoke',
  success: '#4BB543',
};
