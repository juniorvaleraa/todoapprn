import React, {useRef} from 'react';
import {TextInput, View, Text, StyleSheet} from 'react-native';
import {colors, typografy, margin, padding, sizes} from '../../Theme';
import {useForm, Controller} from 'react-hook-form';
import {SafeAreaViewContainer} from '../../components/layouts/SafeAreaViewContainer';
import {PrimaryButton} from '../../components/Buttons/PrimaryButton';
import {taskTitleRules, descriptionTaskRules} from '../../utils/Rules';
import {ErrorMessage} from '../../components/Messages/ErrorMessage';
import {useDispatch} from 'react-redux';
import {editTask} from '../../redux/actions/taskActions';
import {AllDoneIcon, StarIcon} from '../../components/Icons/EvaIcons';
import {ShowToast} from '../../utils/UtilsFuns';

export const ViewAndEditTaskScreen = ({navigation, route}) => {
  const {
    control,
    formState: {errors},
    handleSubmit,
  } = useForm();
  const {title, id, date, description} = route.params;
  const {completed, favorite} = route.params;
  const hasStatus = completed || favorite;

  const TitleRef = useRef(null);
  const DescriptionRef = useRef(null);
  const Dispatch = useDispatch();

  const onEditTask = ({titleTask, descriptionTask}) => {
    Dispatch(
      editTask({
        title: titleTask,
        description: descriptionTask,
        date: new Date(),
        id,
      }),
    );
    ShowToast('success', 'Tarea actualizada', 'Actualizada');
    navigation.navigate('TaskList');
  };

  const StatusIcon = ({children, color}) => {
    return (
      <View style={[styles.statusIconLayout, {backgroundColor: color}]}>
        {children}
      </View>
    );
  };

  return (
    <SafeAreaViewContainer>
      <View style={styles.container}>
        <View style={{marginVertical: margin.normal, flex: 0.4}}>
          <Controller
            control={control}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                placeholder="Titulo de tarea"
                onChangeText={value => onChange(value)}
                value={value}
                ref={TitleRef}
                autoFocus
                maxLength={60}
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: errors.titleTask ? 'red' : 'gray',
                }}
              />
            )}
            name="titleTask"
            defaultValue={title}
            rules={taskTitleRules}
          />
          {errors.titleTask && <ErrorMessage msg={errors.titleTask.message} />}
        </View>

        <View
          style={{
            marginVertical: margin.normal,
            flex: 1,
          }}>
          <Controller
            control={control}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                onChangeText={value => onChange(value)}
                ref={DescriptionRef}
                value={value}
                maxLength={340}
                multiline
                numberOfLines={10}
                onSubmitEditing={handleSubmit(onEditTask)}
                style={{
                  borderWidth: 1,
                  textAlignVertical: 'top',
                  borderRadius: 3,
                  borderColor: errors.descriptionTask ? 'red' : 'gray',
                }}
              />
            )}
            name="descriptionTask"
            rules={descriptionTaskRules}
            defaultValue={description}
          />
          {errors.descriptionTask && (
            <ErrorMessage msg={errors.descriptionTask.message} />
          )}
        </View>
      </View>

      {hasStatus && (
        <View style={styles.statusLayout}>
          <Text style={{fontWeight: 'bold', ...typografy.caption}}>
            Estado:
          </Text>
          {favorite && (
            <StatusIcon color={colors.primary}>
              <StarIcon color="white" size={sizes.normal} />
            </StatusIcon>
          )}
          {completed && (
            <StatusIcon color={colors.success}>
              <AllDoneIcon color="white" size={sizes.normal} />
            </StatusIcon>
          )}
        </View>
      )}

      <View style={styles.dateLayout}>
        <Text style={{fontWeight: 'bold', ...typografy.caption}}>{date}</Text>
      </View>

      <View style={{flex: 0.6}}>
        <PrimaryButton text="Editar" onPress={handleSubmit(onEditTask)} />
      </View>
    </SafeAreaViewContainer>
  );
};

const styles = StyleSheet.create({
  statusIconLayout: {
    height: sizes.normal,
    alignItems: 'center',
    justifyContent: 'center',
    width: sizes.normal,
    borderRadius: sizes.large / 2,
    margin: margin.normal,
  },
  container: {
    flex: 0.75,
    marginVertical: margin.small,
    padding: padding.small,
  },
  dateLayout: {
    position: 'absolute',
    right: 0,
    margin: margin.small,
  },
  statusLayout: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '90%',
    margin: margin.small,
  },
});
