import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Animated,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {SafeAreaViewContainer} from '../../components/layouts/SafeAreaViewContainer';
import {FilterByBar} from '../../components/filter/FilterByBar';
import {useSelector, useDispatch} from 'react-redux';
import {sizes, margin, typografy, colors} from '../../Theme';
import {
  CheckIcon,
  AllDoneIcon,
  CompletedIcon,
  DeleteIcon,
  StarIcon,
} from '../../components/Icons/EvaIcons';
import _ from 'lodash';
import {
  completeTask,
  deleteTask,
  setFavorite,
} from '../../redux/actions/taskActions';
import {ShowToast} from '../../utils/UtilsFuns';

const IMAGE_SIZE = 70;
const PADDING_V = 6;
const PADDING_H = 13;
const MARGIN_V = 10;

const CheckIconBox = ({isSelected, onCheck}) => {
  return (
    <TouchableOpacity
      onPress={onCheck}
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: sizes.small,
        flex: 0.1,
        width: sizes.normal,
        borderRadius: 3,
        borderWidth: 1,
        borderColor: 'gray',
        marginRight: margin.small,
      }}>
      {isSelected && <CheckIcon size={sizes.small} />}
    </TouchableOpacity>
  );
};

const ToolBarBox = ({onPress, right, top, children, color}) => {
  return (
    <Animated.View style={{right, zIndex: 999, top, position: 'absolute'}}>
      <TouchableOpacity
        onPress={onPress}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: color,
          borderRadius: sizes.normal + 2 / 2,
          height: sizes.normal + 2,
          width: sizes.normal + 2,
          zIndex: 999,
        }}>
        {children}
      </TouchableOpacity>
    </Animated.View>
  );
};

export const TaskListScreen = ({navigation}) => {
  const filtered = useSelector(state => state.tasks.filteredTasks);
  const tasks = useSelector(state => state.tasks.tasks);

  const modeRef = useRef(new Animated.Value(0)).current;

  const [selectAllFlag, setSelectAllFlag] = useState(false);
  const [selectedTasks, setSelectedTasks] = useState({});
  const [showFilter, setShowFilter] = useState(false);
  const Dispatch = useDispatch();

  useEffect(() => {
    Animated.sequence([
      Animated.timing(modeRef, {
        toValue: selectAllFlag ? 1 : 0,
        duration: 300,
        useNativeDriver: false,
      }),
    ]).start();
  }, [selectAllFlag]);

  useEffect(() => {
    setSelectedTasks({});
  }, [showFilter]);

  const onSelectAllFlag = () => setSelectAllFlag(prev => !prev);

  const resetFilterState = () => {
    setSelectedTasks({});
    setShowFilter(false);
  };

  const onCheckTask = ({task, isSelected}) => {
    if (isSelected) {
      const newTasks = _.omit(selectedTasks, [task.id]);
      setSelectedTasks(newTasks);
      return;
    }
    setSelectedTasks(prev => ({...prev, [task.id]: task}));
  };

  const onSelectAllTasks = () => {
    let newSelectedTasks = {};

    if (!_.isEmpty(selectedTasks)) {
      setSelectedTasks({});
      return;
    }

    if (showFilter)
      for (let i = 0; i < filtered.length; i++)
        newSelectedTasks = {...newSelectedTasks, [filtered[i].id]: filtered[i]};
    else
      for (let i = 0; i < tasks.length; i++)
        newSelectedTasks = {...newSelectedTasks, [tasks[i].id]: tasks[i]};

    setSelectedTasks(newSelectedTasks);
  };

  const markAllAsFavorite = () => {
    const taskList = _.valuesIn(selectedTasks);
    for (let i = 0; i < taskList.length; i++)
      Dispatch(setFavorite(taskList[i]));
    resetFilterState();
    ShowToast('success', 'Tarea marcada como favorita', 'Favorita');
  };

  const deleteAllSelectedTask = () => {
    const taskList = _.valuesIn(selectedTasks);
    for (let i = 0; i < taskList.length; i++) Dispatch(deleteTask(taskList[i]));
    resetFilterState();
    ShowToast('error', 'Tarea eliminada', 'Eliminada');
  };

  const completeAllSelectedTask = () => {
    const taskList = _.valuesIn(selectedTasks);
    for (let i = 0; i < taskList.length; i++)
      Dispatch(completeTask(taskList[i]));
    resetFilterState();
    ShowToast('success', 'Tarea marcada como completada', 'Completada');
  };

  const goToTask = ({title, id, completed, favorite, description, date}) => {
    const d = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    navigation.navigate('ViewAndEdit', {
      title,
      id,
      completed,
      favorite,
      description,
      date: d,
    });
  };

  const renderItems = ({item, index}) => {
    const isCompleted = item.completed;
    const isSelected = _.has(selectedTasks, item.id);
    return (
      <View style={[styles.cardLayout]}>
        {selectAllFlag && (
          <CheckIconBox
            isSelected={isSelected}
            onCheck={() => onCheckTask({task: item, isSelected})}
          />
        )}
        <TouchableOpacity
          onLongPress={onSelectAllFlag}
          delayLongPress={250}
          onPress={() => goToTask(item)}
          style={styles.cardButtonLayout}>
          <View style={{flex: 0.6}}>
            <Image
              source={{
                uri: 'https://cdn.pixabay.com/photo/2016/03/31/19/58/avatar-1295429_960_720.png',
              }}
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: IMAGE_SIZE,
                height: IMAGE_SIZE,
                borderColor: isCompleted ? colors.success : 'transparent',
                borderWidth: isCompleted ? 3 : 0,
                borderRadius: isCompleted ? IMAGE_SIZE / 2 : 0,
              }}
            />
            {isCompleted && (
              <Text style={styles.completedText}>Completado</Text>
            )}
          </View>

          <View style={{flex: 1}}>
            <Text numberOfLines={1} ellipsizeMode="tail">
              {item.title}
            </Text>
            <Text numberOfLines={3} ellipsizeMode="tail">
              {item.description}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const ToolBar = ({onSelectAll, onComplete, onMarkAsFavorite, onDelete}) => {
    const FavoriteX = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [-wp(53.5), -wp(60.5)],
    });
    const FavoriteY = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [hp(82), hp(74)],
    });
    const CompletedX = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [-wp(53.5), -wp(45.5)],
    });
    const CompletedY = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [hp(82), hp(74)],
    });

    const SelectAllX = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [-wp(53.5), -wp(35.5)],
    });
    const SelectAllY = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [hp(82), hp(78)],
    });

    const DeleteX = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [-wp(53.5), -wp(70.5)],
    });
    const DeleteY = modeRef.interpolate({
      inputRange: [0, 1],
      outputRange: [hp(82), hp(78)],
    });

    return (
      <View style={{position: 'absolute'}}>
        <ToolBarBox
          right={FavoriteX}
          top={FavoriteY}
          onPress={onMarkAsFavorite}
          color={colors.primary}>
          <StarIcon size={sizes.small} color="white" />
        </ToolBarBox>
        <ToolBarBox
          right={DeleteX}
          top={DeleteY}
          onPress={onDelete}
          msg="Eliminar"
          color="red">
          <DeleteIcon size={sizes.small} color="white" />
        </ToolBarBox>
        <ToolBarBox
          right={SelectAllX}
          top={SelectAllY}
          onPress={onSelectAll}
          color="cyan">
          <AllDoneIcon size={sizes.small} color="white" />
        </ToolBarBox>
        <ToolBarBox
          right={CompletedX}
          top={CompletedY}
          onPress={onComplete}
          color={colors.success}>
          <CompletedIcon size={sizes.small} color="white" />
        </ToolBarBox>
      </View>
    );
  };

  return (
    <SafeAreaViewContainer>
      <FilterByBar showFilter={showFilter} setShowFilter={setShowFilter} />

      <ToolBar
        onSelectAll={onSelectAllTasks}
        onMarkAsFavorite={markAllAsFavorite}
        onDelete={deleteAllSelectedTask}
        onComplete={completeAllSelectedTask}
      />

      <View style={{flex: 0.75}}>
        <FlatList
          data={showFilter ? filtered : tasks}
          contentContainerStyle={styles.contentStyle}
          initialNumToRender={3}
          ListEmptyComponent={() => (
            <View>
              <Text style={{...typografy.title}}>Sin Tareas</Text>
            </View>
          )}
          keyExtractor={(item, index) => item.id}
          renderItem={renderItems}
        />
      </View>
    </SafeAreaViewContainer>
  );
};

const styles = StyleSheet.create({
  contentStyle: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexGrow: 1,
  },
  cardButtonLayout: {
    justifyContent: 'space-around',
    alignItems: 'center',
    marginRight: margin.large,
    flexDirection: 'row',
    flex: 1,
  },
  completedText: {
    color: colors.success,
    textTransform: 'uppercase',
    ...typografy.tiny,
    marginTop: margin.small,
  },
  cardLayout: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: MARGIN_V,
    width: '80%',
    height: hp(20),
    backgroundColor: 'rgba(255,255,255,0.8)',
    paddingVertical: PADDING_V,
    paddingHorizontal: PADDING_H,
    elevation: 10,
    borderRadius: 5,
  },
});
