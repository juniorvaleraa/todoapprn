import React, {useRef} from 'react';
import {TextInput, View, Text, StyleSheet} from 'react-native';
import {colors, typografy, margin, padding} from '../../Theme';
import {useForm, Controller} from 'react-hook-form';
import {SafeAreaViewContainer} from '../../components/layouts/SafeAreaViewContainer';
import {PrimaryButton} from '../../components/Buttons/PrimaryButton';
import {taskTitleRules, descriptionTaskRules} from '../../utils/Rules';
import {ErrorMessage} from '../../components/Messages/ErrorMessage';
import {useDispatch} from 'react-redux';
import {createTask} from '../../redux/actions/taskActions';
import {useSelector} from 'react-redux';
import {randomId, randomDate} from '../../utils/UtilsFuns';

export const CreateTaskScreen = ({navigation}) => {
  const {
    control,
    formState: {errors},
    handleSubmit,
  } = useForm();

  const TitleRef = useRef(null);
  const DescriptionRef = useRef(null);
  const Dispatch = useDispatch();

  const onCreateTask = ({title, description}) => {
    const id = randomId();
    const date = randomDate(new Date(2021, 1, 1), new Date());
    Dispatch(createTask({title, id, date, description}));
    TitleRef.current.clear();
    DescriptionRef.current.clear();
    navigation.navigate('TaskList');
  };

  return (
    <SafeAreaViewContainer>
      <View
        style={{
          flex: 0.75,
          marginVertical: margin.large,
          padding: padding.small,
        }}>
        <View style={{marginVertical: margin.normal}}>
          <Controller
            control={control}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                placeholder="Titulo de tarea"
                onChangeText={value => onChange(value)}
                value={value}
                ref={TitleRef}
                autoFocus
                maxLength={60}
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: errors.title ? 'red' : 'gray',
                }}
              />
            )}
            name="title"
            rules={taskTitleRules}
          />
          {errors.title && <ErrorMessage msg={errors.title.message} />}
        </View>

        <View
          style={{
            marginVertical: margin.normal,
          }}>
          <Controller
            control={control}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                onChangeText={value => onChange(value)}
                ref={DescriptionRef}
                value={value}
                maxLength={340}
                multiline
                numberOfLines={10}
                onSubmitEditing={handleSubmit(onCreateTask)}
                style={{
                  borderWidth: 1,
                  textAlignVertical: 'top',
                  borderRadius: 3,
                  borderColor: errors.description ? 'red' : 'gray',
                }}
              />
            )}
            name="description"
            rules={descriptionTaskRules}
            defaultValue=""
          />
          {errors.description && (
            <ErrorMessage msg={errors.description.message} />
          )}
        </View>
        <View style={{flex: 0.6}}>
          <PrimaryButton text="Aceptar" onPress={handleSubmit(onCreateTask)} />
        </View>
      </View>
    </SafeAreaViewContainer>
  );
};
