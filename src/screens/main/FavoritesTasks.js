import React, {useRef} from 'react';
import {
  View,
  Text,
  Image,
  Animated,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {SafeAreaViewContainer} from '../../components/layouts/SafeAreaViewContainer';
import {useSelector, useDispatch} from 'react-redux';
import {sizes, margin, typografy, colors} from '../../Theme';
import {AllDoneIcon, FilledStarIcon} from '../../components/Icons/EvaIcons';
import {deleteFavorite, completeTask} from '../../redux/actions/taskActions';
import _ from 'lodash';
import {ShowToast} from '../../utils/UtilsFuns';

const IMAGE_SIZE = 70;
const PADDING_V = 6;
const PADDING_H = 13;
const MARGIN_V = 10;

const ITEM_SIZE = hp(20) + MARGIN_V;

export const FavoritesTasksScreen = () => {
  const tasks = useSelector(state => state.tasks.tasks);
  const favorites = _.filter(tasks, {favorite: true});
  const scrollY = useRef(new Animated.Value(0)).current;
  const Dispatch = useDispatch();

  const onDeleteFavorite = task => {
    Dispatch(deleteFavorite(task));
    ShowToast('error', 'Tarea ha quitada de favoritos', 'Favorito');
  };

  const onComplete = task => {
    Dispatch(completeTask(task));
    ShowToast('success', 'Tarea marcada como completada', 'Completada');
  };

  const renderItems = ({item, index}) => {
    const isCompleted = item.completed;
    const inputRange = [-1, 0, ITEM_SIZE * index, ITEM_SIZE * (index + 2)];
    const opacityRange = [-1, 0, ITEM_SIZE * index, ITEM_SIZE * (index + 1)];
    const outputRange = [1, 1, 1, 0];
    const scale = scrollY.interpolate({inputRange, outputRange});
    const opacity = scrollY.interpolate({
      inputRange: opacityRange,
      outputRange,
    });
    return (
      <Animated.View
        style={[styles.cardLayout, {opacity, transform: [{scale}]}]}>
        <View style={styles.cardInnerLayout}>
          <View style={{flex: 0.6}}>
            <Image
              source={{
                uri: 'https://cdn.pixabay.com/photo/2016/03/31/19/58/avatar-1295429_960_720.png',
              }}
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: IMAGE_SIZE,
                height: IMAGE_SIZE,
                borderColor: isCompleted ? colors.success : 'transparent',
                borderWidth: isCompleted ? 3 : 0,
                borderRadius: isCompleted ? IMAGE_SIZE / 2 : 0,
              }}
            />
            {isCompleted && (
              <Text style={styles.completedText}>Completado</Text>
            )}
          </View>

          <View style={{flex: 1}}>
            <Text>{item.title}</Text>
            <Text>{item.description}</Text>
          </View>
          <View style={{height: '100%', justifyContent: 'space-between'}}>
            <TouchableOpacity
              onPress={() => onDeleteFavorite(item)}
              style={styles.onDeleteButton}>
              <FilledStarIcon size={sizes.small} color="yellow" />
            </TouchableOpacity>
            {!isCompleted && (
              <TouchableOpacity
                onPress={() => onComplete(item)}
                style={styles.onCompleteButton}>
                <AllDoneIcon size={sizes.small} color="white" />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Animated.View>
    );
  };

  return (
    <SafeAreaViewContainer>
      <View style={styles.titleLayout}>
        <Text style={styles.favoritesTitle}>FAVORITOS</Text>
      </View>
      <View style={{flex: 0.83}}>
        <Animated.FlatList
          data={favorites}
          contentContainerStyle={styles.contentStyle}
          initialNumToRender={3}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {
              useNativeDriver: true,
            },
          )}
          ListEmptyComponent={() => (
            <View>
              <Text>Sin Tareas Favoritas</Text>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItems}
        />
      </View>
    </SafeAreaViewContainer>
  );
};

const styles = StyleSheet.create({
  completedText: {
    color: colors.success,
    textTransform: 'uppercase',
    ...typografy.tiny,
    marginTop: margin.small,
  },

  contentStyle: {
    alignItems: 'center',
    flexGrow: 1,
  },
  cardInnerLayout: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    height: '100%',
  },
  titleLayout: {
    borderRadius: 3,
    width: '80%',
    alignItems: 'center',
    alignSelf: 'center',
  },
  favoritesTitle: {
    fontSize: sizes.normal,
    letterSpacing: 1,
    color: colors.primary,
    fontWeight: 'bold',
  },
  onDeleteButton: {
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    height: sizes.large,
    width: sizes.large,
    borderRadius: sizes.large / 2,
  },
  onCompleteButton: {
    backgroundColor: colors.success,
    alignItems: 'center',
    justifyContent: 'center',
    height: sizes.large,
    width: sizes.large,
    borderRadius: sizes.large / 2,
  },
  cardLayout: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: MARGIN_V,
    width: '80%',
    height: hp(20),
    backgroundColor: 'rgba(255,255,255,0.8)',
    paddingVertical: PADDING_V,
    paddingHorizontal: PADDING_H,
    elevation: 6,
    borderRadius: 5,
  },
});
