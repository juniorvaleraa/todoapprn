import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {MainStackNavigator} from './MainStack';

import {ViewAndEditTaskScreen} from '../screens/main/ViewAndEditTask';

const Stack = createStackNavigator();

export const RootNavigator = () => {
  const options = {
    gestureEnabled: false,
  };

  return (
    <Stack.Navigator headerMode="none" screenOptions={options}>
      <Stack.Screen name="MainNavigator" component={MainStackNavigator} />
      <Stack.Screen name="ViewAndEdit" component={ViewAndEditTaskScreen} />
    </Stack.Navigator>
  );
};
