import React from 'react';
import {TaskListScreen} from '../screens/main/TaskList';
import {CreateTaskScreen} from '../screens/main/CreateTask';
import {FavoritesTasksScreen} from '../screens/main/FavoritesTasks';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {colors, margin, sizes} from '../Theme';
import {StarIcon, ListIcon, PlusIcon} from '../components/Icons/EvaIcons';
import {PlusTabBarButton} from '../components/Buttons/PlusTabBarButton';

const Tab = createBottomTabNavigator();

const options = {
  showLabel: false,
  activeTintColor: colors.primary,
  activeBackgroundColor: colors.light,
  keyboardHidesTabBar: true,
  style: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: margin.small,
    height: sizes.giant,
    elevation: 4,
    marginHorizontal: sizes.normal,
    borderRadius: 6,
  },
};

export const MainStackNavigator = () => (
  <Tab.Navigator
    headerMode="none"
    initialRouteName="TaskList"
    tabBarOptions={options}>
    <Tab.Screen
      name="TaskList"
      component={TaskListScreen}
      options={{
        tabBarLabel: 'Task List',
        tabBarIcon: ({color, size}) => <ListIcon size={size} color={color} />,
      }}
    />
    <Tab.Screen
      name="CreateTask"
      component={CreateTaskScreen}
      options={{
        activeTintColor: colors.primary,
        tabBarIcon: ({size}) => <PlusIcon size={size * 1.2} />,
        tabBarButton: props => <PlusTabBarButton {...props} />,
      }}
    />
    <Tab.Screen
      name="FavoritesTasks"
      component={FavoritesTasksScreen}
      options={{
        tabBarLabel: 'Create Task',
        activeTintColor: colors.primary,
        activeBackgroundColor: colors.light,
        tabBarIcon: ({color, size}) => <StarIcon size={size} color={color} />,
      }}
    />
  </Tab.Navigator>
);
